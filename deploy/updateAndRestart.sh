
#!/bin/bash

# any future command that fails will exit the script
set -e

# Delete the old repo
rm -rf /home/ec2-user/reactproject/

# install git (make sure we have it)
sudo yum install git -y
# clone the repo again
git clone https://gitlab.com/billqueen/reactproject.git
echo "update script git done"
#install node.js, which should install nvm?  from aws tutorial
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.34.0/install.sh | bash
#doesn't work, alternative method
#curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
#sudo apt-get install -y nodejs
#sudo apt-get install -y build-essential
#source the nvm file. In an non
#If you are not using nvm, add the actual path like
# PATH=/home/ubuntu/node/bin:$PATH
source /home/ec2-user/.nvm/nvm.sh
# install node. from AWS tutorial
nvm install node
#check that node is running correctly
node -e "console.log('Running Node.js ' + process.version)"
#install pm2
npm install -g pm2
# stop the previous pm2
pm2 kill
npm remove pm2 -g


#pm2 needs to be installed globally as we would be deleting the repo folder.
# this needs to be done only once as a setup script.
npm install pm2 -g
# starting pm2 daemon
pm2 status

cd /home/ec2-user/reactproject

#install npm packages
echo "Running npm install"
npm install

#Restart the node server
npm start
