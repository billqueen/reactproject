import React from 'react';
import logo from './logo.svg';
import './App.css';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import { Provider } from 'react-redux'
// import store from './store/index'
import LandingPage from './components/landing.js' //.js not necessary
import NewUser from './components/newuser.js' //.js not necessary
import { createStore, compose, applyMiddleware} from "redux";
import allReducers from "./reducers/index";
import thunk from "redux-thunk";

const store = createStore(
    allReducers,
    compose(applyMiddleware(thunk)));

function App() {
  return (

    <div className="App">
      <Provider store={store}>
        <Router>
          <Route path="/" exact component={ LandingPage } /> 
          <Route path="/register" exact component={ NewUser } /> 
        </Router>
      </Provider>
    </div>
  );
}

export default App;
