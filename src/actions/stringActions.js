import * as actionTypes from './actionTypes';


export function setStringOne(string) {
    return function (dispatch) {
        return dispatch({type: actionTypes.SET_STRING_ONE, payload: string })
    }
}

export function setStringTwo(string) {
    return function (dispatch) {
        return dispatch({type: actionTypes.SET_STRING_TWO, payload: string })
    }
}
