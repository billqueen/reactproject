import React, { Component } from 'react'; 
import { BrowserRouter as Router, Route, Link, Redirect } from "react-router-dom";
import { connect } from 'react-redux'; 
import { bindActionCreators} from 'redux'; 
import { Jumbotron, Container, Button } from 'reactstrap';
import * as stringActions from '../actions/stringActions'; 

class LandingPage extends Component {
  constructor(props) {
    super(props)
    this.state={
      name: 'Bill Queen'
    }
    this.goToRegisterNewUser = this.goToRegisterNewUser.bind(this)
  }
  goToRegisterNewUser(){
    console.log("newuser function hit")
    return <Redirect push to={'/register'} /> 
  }

  render() {
    //console.log(this.props.firststring)
    return(
      <div>
        <Jumbotron fluid>
          <Container fluid>
            <h1 className="display-3">{this.state.name}</h1>
            <p className="lead">This is a modified jumbotron that occupies the entire horizontal space of its parent.</p>
            <Link to="/register">Register</Link>
          </Container>
        </Jumbotron>
      </div>
    )
  }
}

//function mapStateToProps(state, ownProps) {
//  return {
//    firststring: state.stringReducer.stringOne,
//    secondstring: state.stringReducer.stringTwo
//  }
//}
//function mapDispatchToProps(dispatch) {
//  return {
//    actions: bindActionCreators(Object.assign(stringActions), dispatch)
//  };
//}

export default LandingPage;
