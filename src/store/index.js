import { createStore, compose, applyMiddleware} from "redux/index"; 
import allReducers from "../reducers/index"; 
import thunk from "redux-thunk/index"; 

const store = createStore(
  allReducers, 
  compose(applyMiddleware(thunk))); 

export default store; 
